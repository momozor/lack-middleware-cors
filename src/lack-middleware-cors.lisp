(defpackage lack.middleware.cors
  (:use :cl)
  (:import-from :lack.response
                :response-headers)
  (:export :*lack-middleware-cors*))
(in-package :lack.middleware.cors)

(defvar *lack-middleware-cors*
  (lambda (app &key
                 (origin "*")
                 (methods "GET, POST, OPTIONS")
                 (max-age 1000))
    (lambda (env)
      (let ((response (getf env :path-info)))
        (funcall env `(200
                       (:content-type "text/plain")
                       (,response))))
      (funcall app env))))
