(defsystem "lack-middleware-cors"
  :version "0.1.0"
  :author "Momozor"
  :license "MIT"
  :depends-on ("lack"
               "cl-ppcre")
  :components ((:module "src"
                        :components
                        ((:file "lack-middleware-cors"))))
  :description "Lack middleware for enabling CORS"
  :in-order-to ((test-op (test-op "lack-middleware-cors/tests"))))

(defsystem "lack-middleware-cors/tests"
  :author "Momozor"
  :license "MIT"
  :depends-on ("lack-middleware-cors"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for lack-middleware-cors"
  :perform (test-op (op c) (symbol-call :rove :run c)))
