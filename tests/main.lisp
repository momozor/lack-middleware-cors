(defpackage lack-middleware-cors/tests/main
  (:use :cl
        :lack-middleware-cors
        :rove))
(in-package :lack-middleware-cors/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :lack-middleware-cors)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
